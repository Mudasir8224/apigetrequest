package com.example.apigetrequest;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ApiResponse<T> {

    @SerializedName("data")
    List<T> usersRecord;

    @SerializedName("status")
    String status;

    public ApiResponse(List<T> usersRecord, String status) {
        this.usersRecord = usersRecord;
        this.status = status;
    }

    public List<T> getUsersRecord() {
        return usersRecord;
    }

    public String getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return "ApiResponse{" +
                "usersRecord=" + usersRecord +
                ", status='" + status + '\'' +
                '}';
    }
}
