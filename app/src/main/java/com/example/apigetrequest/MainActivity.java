package com.example.apigetrequest;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiInterface.baseURL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        ApiInterface apiInterface = retrofit.create(ApiInterface.class);
        Call<ApiResponse<UsersData>> call = apiInterface.getUsersDataInObjectOfArray();
        call.enqueue(new Callback<ApiResponse<UsersData>>() {
            @Override
            public void onResponse(Call<ApiResponse<UsersData>> call, Response<ApiResponse<UsersData>>response) {
                Log.d("Call","onResponse"+response.body().toString());
            }

            @Override
            public void onFailure(Call<ApiResponse<UsersData>> call, Throwable t) {
                Log.d("Call","onFailure"+t.getMessage());
            }
        });

        /*Json Array data response
        Call<List<UsersData>> call = apiInterface.getUsersArray();
        call.enqueue(new Callback<List<UsersData>>() {
            @Override
            public void onResponse(Call<List<UsersData>> call, Response<List<UsersData>> response) {
                Log.d("Call","onResponse"+response.body().toString());
            }

            @Override
            public void onFailure(Call<List<UsersData>> call, Throwable t) {
                Log.d("Call","onFailure"+t.getMessage());

            }
        });

         */

        /*Json Object data Response
        Call<UsersData> call = apiInterface.getUsers();
        call.enqueue(new Callback<UsersData>() {
            @Override
            public void onResponse(Call<UsersData> call, Response<UsersData> response) {

                UsersData call1 = response.body();
                Log.d("chal ba",call1.body);

              //  Log.d("Call","onResponse :"+ response.body().title);
              //  Log.d("Call","onResponse :"+ response.body().userId);
               // Log.d("Call","onResponse :"+ response.body().body);
                Log.d("Call","onResponse :"+ response.body().toString());
            }

            @Override
            public void onFailure(Call<UsersData> call, Throwable t) {

                Log.d("Call","onFailure"+t.getMessage());

            }
        });

         */


    }
}
