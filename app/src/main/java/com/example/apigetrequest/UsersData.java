package com.example.apigetrequest;

import com.google.gson.annotations.SerializedName;

public class UsersData {

    @SerializedName("id")
    String id;

    @SerializedName("employee_name")
    String employee_name;

    @SerializedName("employee_salary")
    String employee_salary;

    @SerializedName("employee_age")
    String employee_age;

    @SerializedName("profile_image")
    String profile_image;

    public UsersData(String id, String employee_name, String employee_salary, String employee_age, String profile_image) {
        this.id = id;
        this.employee_name = employee_name;
        this.employee_salary = employee_salary;
        this.employee_age = employee_age;
        this.profile_image = profile_image;
    }

    @Override
    public String toString() {
        return "UsersData{" +
                "id='" + id + '\'' +
                ", employee_name='" + employee_name + '\'' +
                ", employee_salary='" + employee_salary + '\'' +
                ", employee_age='" + employee_age + '\'' +
                ", profile_image='" + profile_image + '\'' +
                '}';
    }

    /*Json placeholder api
    @SerializedName("userId")
    String userId;
    @SerializedName("id")
    String id;
    @SerializedName("title")
    String title;
    @SerializedName("body")
    String body;

    public UsersData(String userId, String id, String title, String body) {
        this.userId = userId;
        this.id = id;
        this.title = title;
        this.body = body;
    }

    @Override
    public String toString() {
        return "UsersData{" +
                "userId='" + userId + '\'' +
                ", id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", body='" + body + '\'' +
                '}';
    }

    */


}
