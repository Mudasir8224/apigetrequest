package com.example.apigetrequest;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiInterface {

       String baseURL = "https://dummy.restapiexample.com/";

       @GET("/api/v1/employees")
       Call <ApiResponse<UsersData>> getUsersDataInObjectOfArray();


          /*Json placeholder api
       String baseURL = "https://jsonplaceholder.typicode.com/";

           */

            /*Json Array data response
       @GET ("/posts")
       Call<List<UsersData>> getUsersArray();

             */

       /*Json Object Response
       @GET("/posts/1")
       Call<UsersData> getUsers();

        */

}
